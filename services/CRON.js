
const { Script, Cron } = require('../models/sequelize')

const mqttService = require("../services/mqtt");

const node_cron = require('node-cron');


function refreshCron() {
    console.log('✅ [CRON] Refreshing cron');

    //FIXME: Должно быть функция для переиспользования


    // TODO: Нет удаления после выполнения
    // TODO: Добавить дополнительные функции типа - повторени по дням по часам и так далее
    Cron.findAll({
        raw: true
    }).then(crons => {
        crons.forEach(cron => {
            Script.findAll({
                raw: true,
                where: {
                    script_id: cron.script_id
                }
            }).then(scripts => {
                scripts.forEach(script => {

                    var ext_cron = initStringOfCron(cron)
                    console.log("✅ [CRON] New cron added to queue " + cron.cron_id + " [" + ext_cron + "]");

                    const startupTask = node_cron.schedule(ext_cron, () => {
                        console.log("✅ [CRON] Script executed " + script.script_description);
                        // TODO: Отправка PUSH сообщений о выполнении скрипты
                        mqttService.publishToTopic(script.script_than, script.script_than_that);
                    });
                    
                });
            }).catch(err => console.log(err));
        });
    }).catch(err => console.log(err));
}

refreshCron();

module.exports.createCronPost = function (script) {
    Cron.create({
        script_id: script.script_id, // TODO: Cron с ID скрипта к которому относится
        cron_dayOfWeek: script.script_datetime.getUTCDay(),
        cron_month: script.script_datetime.getUTCMonth(),
        cron_dayOfMonth: script.script_datetime.getUTCDate(),
        cron_year: script.script_datetime.getUTCFullYear(),
        cron_hour: script.script_datetime.getUTCHours() + 7, // FIXME: Пофиксить временные зоны, а то время пишет 25 часов
        cron_minute: script.script_datetime.getUTCMinutes(),
        cron_utc: script.script_datetime.getTimezoneOffset(), // FIXME: Не то что нужно
    }).then(cron => {
        refreshCron();
    })
};

module.exports.deleteCronId = function (script_id) {
    Cron.destroy({
        where: {
            script_id: script_id
        }
    }).then((res) => {

        console.log('✅ [CRON] Cron deleted');
        refreshCron();
    });
};

module.exports.editCron = function (script_id) {
    console.log(script_id);

    //FIXME: Убрать поиска скрипта здесь данные должны передаваться напрямую
    Script.findOne({
        where:
        {
            script_id: script_id
        }
    }).then(script => {
        // должно быть
        Cron.update({
            cron_dayOfWeek: script.script_datetime.getUTCDay(),
            cron_month: script.script_datetime.getUTCMonth(),
            cron_dayOfMonth: script.script_datetime.getUTCDate(),
            cron_year: script.script_datetime.getUTCFullYear(),
            cron_hour: script.script_datetime.getUTCHours() + 7, // FIXME: Пофиксить временные зоны, а то время пишет 25 часов
            cron_minute: script.script_datetime.getUTCMinutes(),
            cron_utc: script.script_datetime.getTimezoneOffset(),
        }, {
            where: {
                script_id: script.script_id
            }
        }).then((res) => {

            console.log('✅ [CRON] Cron was changed');
            refreshCron();
        });
        // -----
    }).catch(err => console.log(err));
};

// TODO: Добавить дополнительные параметры
function initStringOfCron(cron) {
    return cron.cron_minute + ' ' +
        cron.cron_hour + ' ' +
        '* ' +
        '* ' +
        '* ';
}