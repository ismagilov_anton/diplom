

const { Log, Device, Script, User } = require("../../../models/sequelize");

const path = require('path');
const mqttService = require("../../mqtt");

const Email = require('../../../node_modules/email-templates');
const nodemailer = require('../../../node_modules/nodemailer');

const email = new Email({
  message: {
    from: process.env.MAIL_USER
  },
  // uncomment below to send emails in development/test env:
  // send: true,
  views: {
    options: {
      extension: 'hbs' // <---- HERE
    }
  },
  preview: {
    open: {
      app: 'msedge',
      wait: false
    }
  },
  webResources: {
    // FIXME: Исправить расположение стиля для письма
    // relativeTo: path.resolve('emails')
    relativeTo: path.join(__dirname, "../../../" , "emails")

  },
  transport: {
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    auth: {
      user: process.env.MAIL_USER,
      pass: process.env.MAIL_PASS,
    },
    tls: {
      rejectUnauthorized: false
    }
  }
});









// Добавить timestamp

module.exports.Guard = function (script, topic, message) {
  console.log("✅ [SCRIPT] You should call Guard Caller");



  if (verifyCondition(script, topic, message) == true) {
    console.log("✅ [SCRIPT] Expression of script right");
    sendEmail(script)
  }
  else
    console.log("❌ [SCRIPT] Expression of script wrong");

};


module.exports.Classic = function (script, topic, message) {
  console.log("✅ [SCRIPT] You should call Classic Caller");

  var data = JSON.parse(message)

  if (verifyCondition(script, topic, message) == true) {
    console.log("✅ [SCRIPT] Expression of script right");
    mqttService.publishToTopic(script.script_than, script.script_than_that);
  }
  else
    console.log("❌ [SCRIPT] Expression of script wrong");
}

module.exports.Comfort = function (topic, message) {
  console.log("✅ [SCRIPT] You should call Comfort Caller");
  // Регулировка температуры и прочего 
};




function sendEmail(script) {
  User.findOne({
    where:
    {
      id: script.user_id
    }
  }).then(user => {

    email.send({
      template: script.script_type,
      message: {
        to: user.email
      },
      locals: {
        name: user.lastname,
        script: script
      }
    })
      .then(console.log("✅ [MAIL] Mail was sended, script execution"))
      .catch(console.error);
  }).catch(err => console.log(err));
}

function verifyCondition(script, topic, message) {
  var data = JSON.parse(message)

  switch (script.script_comparison) {
    case "less":
      if (data.device_value < script.script_if_that) {
        console.log(data.device_value + script.script_comparison + script.script_if_that + "RIGHT");
        return true;
      }
      else {
        console.log(data.device_value + script.script_comparison + script.script_if_that + "WRONG");
        return false;
      }
      break;
    case "more":
      if (data.device_value > script.script_if_that) {
        console.log(data.device_value + script.script_comparison + script.script_if_that + "RIGHT");
        return true;
      }
      else {
        console.log(data.device_value + script.script_comparison + script.script_if_that + "WRONG");
        return false;
      }
      break;
    case "equally":
      if (data.device_value = script.script_if_that) {
        console.log(data.device_value + script.script_comparison + script.script_if_that + "RIGHT");
        return true;
      }
      else {
        console.log(data.device_value + script.script_comparison + script.script_if_that + "WRONG");
        return false;
      }
      break;

    default:
      break;
  }


}