const { User, Device } = require("../../../models/sequelize");

// const fs = require('fs');

// var content = fs.readFileSync("device-support.json", "utf8");
// var device_support = JSON.parse(content);


// var sensors = [];



// for (var i in device_support.devices.sensor)
//     sensors.push(device_support.devices.sensor[i].hello.device_type);


module.exports.User = function (client) {
    // Подписаться на все приватные топики пользователей при запуске сервера
    User.findAll({
        raw: true
    })
        .then((users_topics) => {
            for (let index = 0; index < users_topics.length; index++) {
                const element = users_topics[index].private_topic;
                client.subscribe(element);
                console.log("✅ [TOPIC-USER] Subcribed " + element);
            }
        })
        .catch((err) => console.log(err));
};

module.exports.Device = function (client) {
    // Подписаться на все топики устройств от которых должны приходить данные
    Device.findAll({
        raw: true,
        // Подписка на всех или на определенных
        // where: {
        //     device_type: sensors,
        // },
    })
        .then((devices) => {
            for (let index = 0; index < devices.length; index++) {
                const element = devices[index].device_topic;
                client.subscribe(element);
                console.log("✅ [TOPIC-DEVICE] Subcribed " + element);
            }
        })
        .catch((err) => console.log(err));
};