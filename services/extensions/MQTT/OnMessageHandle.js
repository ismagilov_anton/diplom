const { Device, Script, User, Log } = require("../../../models/sequelize");

const handleScript = require("./HandleScript.js");

module.exports.Connect = function (client, topic, message) {
  var data = JSON.parse(message);
  //FIXME: Добавляет несмторя на то что уже есть в системе
  User.findOne({
    raw: true,
    where: {
      private_topic: topic,
    },
  }).then((user) => {
    Device.findOrCreate({
      raw: true,
      where: {
        user_id: user.id,
        device_topic: data.device_topic,
        device_type: data.device_type,
      },
    })
      .then(() => {
        console.log("✅ [DEVICE] New device " + data.device_topic + " added for" + user.username);
        client.subscribe(data.device_topic);
        console.log("✅ [MQTT] Subscribed " + data.device_topic);
      })
      .catch((err) => console.log(err));
  })
    .catch((err) => console.log(err));
};

//sensors
module.exports.Event = function (topic, message) {

  console.log(topic)
  console.log(JSON.parse(message));

  createLog(topic, message);
  checkForScript(topic, message);

  console.log("⚠️ Были получены данные от сенсора");
};


//actuators
module.exports.Command = function (topic, message) {
  console.log(topic)
  console.log(JSON.parse(message));

  createLog(topic, message);
  checkForScript(topic, message);
  console.log("⚠️ Были получены данные от исполнительного устройства");
}

function createLog(topic, message) {
  var data = JSON.parse(message);

  Device.findOne({
    raw: true,
    where: {
      device_topic: topic,
    },
  })
    .then((device) => {
      return Log.create({
        device_id: device.device_id,
        log_value: data.device_value,
      })
        .then((log) => {
          console.log("✅ [LOG-SENSOR] New log of " + device.device_name + " added");
          return null;
        })
        .catch((err) => console.log(err));
    })
    .catch((err) => console.log(err));
}

// TODO: Скрипты на акутаторы - если что то включилось сделать чтото по аналогии с Guard
function checkForScript(topic, message) {

  Script.findAll({
    raw: true,
    where: {
      script_if: topic,
    },
  })
    .then((scripts) => {
      scripts.forEach((script) => {
        switch (script.script_type) {
          case "guard":
            {
              handleScript.Guard(script, topic, message);
            }
            break;
          case "comfort":
            {
              handleScript.Comfort(script, topic, message);
            }
            break;
          case "classic":
            {
              handleScript.Classic(script, topic, message);
            }
            break;
          default:
            break;
        }
      });
    })
    .catch((err) => console.log(err));
}