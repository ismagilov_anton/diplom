const { Log, Device, Script, User } = require("../models/sequelize");
const mqtt = require("mqtt");
const client = mqtt.connect("mqtt://broker.mqtt-dashboard.com");

const onMessageHandle = require("./extensions/mqtt/OnMessageHandle");
const onConnectStartup = require("./extensions/mqtt/OnConnectStartup");

const comfort = require('./comfort');


//FIXME: Все что связано с mqtt выполняется два раза, без понятия почему



// TODO: Добавтиь защиту сообщений
// TODO: Добавить работу с флагами сообщений
// TODO: Lightweight Machine-to-Machine (LWM2M)
// TODO: Сохранять текущее состояние ИУ Если включен с кнопки?

// Подписаться на топик при регистрации пользователя
module.exports.subscribeTopic = function (topic) {
  client.subscribe(topic);
  console.log("✅ [PASSPORT] Subscribed " + topic + " while registration");
};

module.exports.unsubscribeTopic = function (topic) {
  client.unsubscribe(topic);
  console.log("✅ [MQTT] Unsubscribed " + topic);
};

module.exports.publishToTopic = function (topic, value) {
  client.publish(topic, value);
  console.log("✅ [MQTT] Message " + value + " sended to " + topic);
}

client.on("connect", () => {
  // Подписаться на все приватные топики пользователей при запуске сервера
  onConnectStartup.User(client);
  // Подписаться на все топики устройств от которых должны риходить данные
  onConnectStartup.Device(client);
});

// Прослушиватель сообщений, действие при публикации
client.on("message", (topic, message) => {

  var arrayOfStrings = topic.split("/");
  var theme = arrayOfStrings[2];

  // console.log(theme);

  switch (theme) {
    case "connected":
      onMessageHandle.Connect(client, topic, message);
      break;


    case "events":
      onMessageHandle.Event(topic, message);
      break;

    //TODO: Что то пришло на акутатор мы это получили
    case "commands":
      onMessageHandle.Command(topic, message);
      break;
    default:
      break;
  }
});


// TODO: Добавить сервис которые будет следить за выполнением ГОСТОВ по комфортности среды