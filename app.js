const express = require("express");
var bodyParser = require('body-parser')

const app = express();
var flash = require("connect-flash");

require('dotenv').config()
const webpush = require('web-push');

var passport = require('passport')
var session = require('express-session')
var bodyParser = require('body-parser')
const hbs = require("hbs");



const fs = require('fs')
const https = require('https')



//For BodyParser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// For Passport
app.use(
    session(
        { secret: process.env.SESSION_SECRET, resave: true, saveUninitialized: true }
    )
); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions

app.use(flash());
app.use((req, res, next) => {
    res.locals.error_messages = req.flash('error')
    next()
})

// HBS
app.set("view engine", "hbs");
hbs.registerPartials(__dirname + "/views/layouts");


app.use(express.static(__dirname + '/public'));
app.use(express.static('public/img'));

// Models
const { User } = require('./models/sequelize')
require('./config/passport/passport.js')(passport, User);

//Routes
const homeRouter = require("./routes/homeRouter.js")(app);
const scriptRouter = require("./routes/scriptRouter.js")(app);
const logRouter = require("./routes/logRouter.js")(app);
const deviceRouter = require("./routes/deviceRouter.js")(app);
const authRouter = require('./routes/authRouter.js')(app, passport);


app.use("/", homeRouter);
app.use("/auth", authRouter);
app.use("/logs", logRouter);
app.use("/script", scriptRouter);
app.use("/device", deviceRouter);


app.use(function (request, response, next) {
    // res.status(404).send("Not Found");
    response.render("HomeError.hbs");
});
app.get('/', function (request, response) {
    response.render("HomeMain.hbs");
})


app.listen(process.env.PORT, function () {
    console.log(`✅ [SERVER] Listening on port ${process.env.PORT}!`)
})






// ООП
// Исключения и проверки
// Тестирование
// UML диаграмму
// Шаблон проектирования
// Схему элементов проекта
// Моудльное программирование

// Что я сделал с Metanit

//TODO: npx arkit diplom/ -o arkit.svg
//TODO: npx arkit diplom/ -o arkit.svg


//TODO: Применить множественные popup окна к списку лога и скриптов - это сокртитвзаимодейтсиве
// script_list уже имеет начало

// TODO: Пофиксить Promices
// TODO: Все выполняется два раза




