function openNav() {
    document.getElementById("mySidenav").style.width = "400px";
    document.getElementById("main").style.marginLeft = "400px";
    document.getElementById("topnav").style.marginLeft = "400px";
    document.getElementsByClassName("sidenav_icons").style.visibility = "visible";
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
    document.getElementById("topnav").style.marginLeft = "0";
    document.getElementsByClassName("sidenav_icons").style.visibility = "hidden";
    document.body.style.backgroundColor = "white";
}