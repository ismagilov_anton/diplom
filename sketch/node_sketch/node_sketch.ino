#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#include <ArduinoJson.h>
#include <SPI.h>
#include <DHT.h>

#define SENSOR 2 // Signal pin connected to D4 of nodemcu
#define ACTUATOR 1

// Uncomment whatever type you're using!
// #define DHTTYPE DHT11 // DHT 11
#define DHTTYPE DHT22   // DHT 22, AM2302, AM2321
//#define DHTTYPE DHT21   // DHT 21, AM2301

DHT dht(SENSOR, DHTTYPE);

// Create a random client ID
    String clientId = "ESP8266Client-" + String(random(0xffff), HEX);
// Update these with values suitable for your network.

String ssid = "MI A2";
String password = "44743352";

String private_topic = "devices/3f45ba9/connected";

// FIXME Изменить генеацию случайного имени
String actuator_topic = "devices/"+clientId+"/commands";
String actuator_type = "actuator_siren";
    
String sensor_topic = "devices/"+clientId+"/events";
String sensor_type = "sensor_temperature";

String mqtt_server = "broker.mqtt-dashboard.com";

WiFiClient espClient;
PubSubClient client(espClient);


// STANDART
void setup(){
  // Установка подключения датчиков и ИУ
  pinMode(ACTUATOR, OUTPUT); // Initialize the BUILTIN_LED pin as an output
  // -----
  Serial.begin(115200);
  

  // Подкчлюение к сети
  wifi_setup();
  client.setServer(mqtt_server.c_str(), 1883);
  client.setCallback(mqtt_callback);
  // -----
  
}
void loop(){

  if (!client.connected())
  {
    wifi_reconnect();
  }

  client.loop();
  client.setCallback(mqtt_callback);

  mqtt_sendStateSensor();
}
// -----

// WIFI
void wifi_setup(){
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("[WIFI] Connecting to ");
  Serial.println(ssid.c_str());

  WiFi.begin(ssid.c_str(), password.c_str());

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println();
  Serial.println("[WIFI] WiFi connected");
  Serial.println("[WIFI] IP address: ");
  Serial.println(WiFi.localIP());
}
void wifi_reconnect(){
  // Loop until we're reconnected
  while (!client.connected())
  {
    Serial.print("[MQTT] Attempting MQTT connection...");

    // Attempt to connect
    if (client.connect(clientId.c_str()))
    {

      Serial.println();
      Serial.println("[MQTT] Connected");
      // Once connected, publish an announcement...

    //FIXME: Функции отправляющие привет должны быть в подключении к сети
      mqtt_sendHelloSensor();
      mqtt_sendHelloActuator();

    }
    else
    {
      Serial.print("[MQTT] Failed, reconnect = ");
      Serial.print(client.state());
      Serial.println("[MQTT] Try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
// ------

// MQTT
void mqtt_callback(char *topic, byte *payload, unsigned int length){
  //TODO: Добавить парсинг JSON
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");

  for (int i = 0; i < length; i++)
  {
    Serial.print((char)payload[i]);
  }

  // TODO: Обработка топика JSON
  Serial.println((char)payload[0]);

  switch ((char)payload[0])
  {
  case '1':
  {
    Serial.println("[ACTUATOR] ON");
    digitalWrite(ACTUATOR, LOW);
    break;
  }
  case '0':
  {
    Serial.println("[ACTUATOR] OFF");
    digitalWrite(ACTUATOR, HIGH);
    break;
  }
  default:
    break;
  }

  Serial.println();
}
void mqtt_sendStateSensor(){
  // JSON Package
  StaticJsonBuffer<300> JSONbuffer;
  JsonObject &JSONencoder = JSONbuffer.createObject();

  float data = dht.readTemperature(); // or dht.readTemperature(true) for Fahrenheit

  JSONencoder["device_value"] = data;

  char device_value[100];
  JSONencoder.printTo(device_value, sizeof(device_value));

  if (isnan(data)){
    Serial.println("[SENSOR] Failed to read from DHT sensor!");
    delay(10000);
    return;
  }

  if (client.publish(sensor_topic.c_str(), device_value, false) == true)
  {
    Serial.println("[MQTT] Success sending message");
    Serial.println(device_value);
  }
  else
  {
    Serial.println("[MQTT] Error sending message");
  }

  delay(10000);
}
void mqtt_sendHelloSensor(){
      // Sensor
      char HelloSensor[100];
      StaticJsonBuffer<300> JSONbufferSensor;
      JsonObject &JSONhelloSensor = JSONbufferSensor.createObject();
      JSONhelloSensor["device_topic"] = sensor_topic;
      JSONhelloSensor["device_type"] = sensor_type;
      JSONhelloSensor.printTo(HelloSensor, sizeof(HelloSensor));
      Serial.println(HelloSensor);
      client.publish(private_topic.c_str(), HelloSensor);
      client.subscribe(sensor_topic.c_str()); //For callback
}
void mqtt_sendHelloActuator(){
      // Actuator
      char HelloActuator[100];
      StaticJsonBuffer<300> JSONbufferActuator;
      JsonObject &JSONhelloActuator = JSONbufferActuator.createObject();
      JSONhelloActuator["device_topic"] = actuator_topic;
      JSONhelloActuator["device_type"] = actuator_type;
      JSONhelloActuator.printTo(HelloActuator, sizeof(HelloActuator));
      Serial.println(HelloActuator);
      client.publish(private_topic.c_str(), HelloActuator);
      client.subscribe(actuator_topic.c_str());
}
// ------