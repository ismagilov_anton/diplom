//load bcrypt
var bCrypt = require('bcrypt-nodejs');
const mqttService = require("../../services/MQTT");


module.exports = function (passport, user) {

    var User = user;
    var LocalStrategy = require('passport-local').Strategy;

    passport.use('local-signup', new LocalStrategy(
        {
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function (request, email, password, done) {
            var generateHash = function (password) {
                return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
            };

            User.findOne({
                where: {
                    email: email
                }
            }).then(function (user) {
                if (user) {
                    // return done(null, false, {
                    //     message: 'That email is already taken'
                    // });
                    return done(null, false, request.flash('error', 'That email is already taken'));
                } else {
                    var userPassword = generateHash(password);
                    const topic = `${(~~(Math.random() * 1e8)).toString(16)}`;
                    var privateTopic = "devices/" + topic + "/connected";

                    mqttService.subscribeTopic(privateTopic);

                    var data =
                    {
                        email: email,
                        password: userPassword,
                        firstname: request.body.firstname,
                        lastname: request.body.lastname,
                        username: request.body.username,
                        private_topic: privateTopic
                    };

                    User.create(data).then(function (newUser, created) {
                        if (!newUser) {
                            return done(null, false);
                        }
                        if (newUser) {
                            return done(null, newUser);
                        }
                    });
                }
            });
        }
    ));

    //LOCAL SIGNIN
    passport.use('local-signin', new LocalStrategy(

        {
            // by default, local strategy uses username and password, we will override with email
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function (request, email, password, done) {
            var User = user;
            var isValidPassword = function (userpass, password) {
                return bCrypt.compareSync(password, userpass);
            }

            User.findOne({
                where: {
                    email: email
                }
            }).then(function (user) {

                if (!user) {
                    return done(null, false, {
                        message: 'Email does not exist'
                    });
                    return done(null, false, request.flash('error', 'Email does not exist'));

                }

                if (!isValidPassword(user.password, password)) {
                    // return done(null, false, {
                    //     message: 'Incorrect password.'
                    // });
                    return done(null, false, request.flash('error', 'Incorrect password.'));

                }

                var userinfo = user.get();
                return done(null, userinfo);

            }).catch(function (err) {
                console.log("Error:", err);
                // return done(null, false, {
                //     message: 'Something went wrong with your Signin'
                // });
                return done(null, false, request.flash('error', 'Something went wrong with your Signin'));

            });
        }
    ));

    //serialize
    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });

    // deserialize user 
    passport.deserializeUser(function (id, done) {
        User.findOne({
            where: {
                id: id
            }
        }).then(function (user) {
            if (user) {
                done(null, user.get());
            } else {
                done(user.errors, null);
            }
        });
    });
}