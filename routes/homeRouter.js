const express = require("express");
const homeController = require("../controllers/homeController.js");
const homeRouter = express.Router();

// homeRouter.get("/about", homeController.about);
// homeRouter.get("/", homeController.index);
// homeRouter.get("/main", homeController.main);
// homeRouter.get("/test", homeController.test);

module.exports = homeRouter;

// ------

module.exports = function (app, passport) {


    app.get("/profile", isLoggedIn, homeController.profile);
    app.get("/main", isLoggedIn, homeController.main);
    app.get("/test", isLoggedIn, homeController.test);


    function isLoggedIn(req, res, next) {

        if (req.isAuthenticated())
            return next();
        res.redirect('/signin');
    }

    return homeRouter;
}