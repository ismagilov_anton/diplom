const express = require("express");
const scriptController = require("../controllers/scriptController.js");

const scriptRouter = express.Router();


module.exports = scriptRouter;

module.exports = function (app, passport) {


    // Получить все скрипты
    app.get("/script", isLoggedIn, scriptController.getScript);

    // Создать скрипт
    app.get("/script/create", isLoggedIn, scriptController.createScript);

    app.post("/script/create", isLoggedIn, scriptController.createScriptPost);

    // Удалить скрипт
    app.post("/script/delete/:script_id", isLoggedIn, scriptController.deleteScriptId);

    // Изменить скрипт
    app.get("/script/edit/:script_id", isLoggedIn, scriptController.editScriptId);
    app.post("/script/edit", isLoggedIn, scriptController.editScript);



    function isLoggedIn(req, res, next) {

        if (req.isAuthenticated())
            return next();
        res.redirect('/signin');
    }

    return scriptRouter;
}