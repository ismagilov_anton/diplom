const express = require("express");
const deviceController = require("../controllers/deviceController.js");

const deviceRouter = express.Router();


module.exports = deviceRouter;

module.exports = function (app, passport) {


    // Получить все скрипты
    app.get("/device", isLoggedIn, deviceController.getDevice);

    // Создать скрипт
    // app.get("/device/create", isLoggedIn, deviceController.createDevice);

    // app.post("/device/create", isLoggedIn, deviceController.createDevicePost);

    // Удалить скрипт
    app.post("/device/delete/:device_id", isLoggedIn, deviceController.deleteDeviceId);

    // Изменить скрипт
    app.get("/device/edit/:device_id", isLoggedIn, deviceController.editDeviceId);
    app.post("/device/edit", isLoggedIn, deviceController.editDevice);



    function isLoggedIn(req, res, next) {

        if (req.isAuthenticated())
            return next();
        res.redirect('/signin');
    }

    return deviceRouter;
}