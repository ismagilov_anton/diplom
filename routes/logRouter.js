const express = require("express");
const logController = require("../controllers/logController.js");
const logRouter = express.Router();



module.exports = logRouter;

module.exports = function (app, passport) {


    // Показать всех датчики
    app.get("/logs", isLoggedIn, logController.getLog);

    app.get("/logs/show/:id", isLoggedIn, logController.getLogId);

    function isLoggedIn(req, res, next) {

        if (req.isAuthenticated())
            return next();
        res.redirect('/signin');
    }

    return logRouter;
}