const express = require("express");
var authController = require('../controllers/authController.js');

const authRouter = express.Router();

module.exports = function (app, passport) {
    app.get('/signup', authController.signup);

    app.get('/signin', authController.signin);

    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect: '/main',
        failureFlash: true,
        failureRedirect: '/signup',
    }
    ));

    app.post('/signin', passport.authenticate('local-signin', {
        successRedirect: '/main',
        failureFlash: true,
        failureRedirect: '/signin',
    }
    ));
    app.get("/", authController.index);

    app.get('/logout', authController.logout);

    function isLoggedIn(req, res, next) {

        if (req.isAuthenticated())
            return next();
        res.redirect('/signin');
    }

    return authRouter;
}