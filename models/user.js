module.exports = (sequelize, type) => {

    const TIMESTAMP = require('sequelize-mysql-timestamp')(sequelize);


    return sequelize.define('user', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: type.INTEGER
        },

        firstname: {
            type: type.STRING,
            notEmpty: true
        },

        lastname: {
            type: type.STRING,
            notEmpty: true
        },

        username: {
            type: type.TEXT
        },
        email: {
            type: type.STRING,
            validate: {
                isEmail: true
            }
        },
        password: {
            type: type.STRING,
            allowNull: false
        },
        private_topic: {
            type: type.STRING,
            allowNull: false
        },
        last_login: {
            type: TIMESTAMP,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
            allowNull: false
        },

        status: {
            type: type.ENUM('active', 'inactive'),
            defaultValue: 'active'
        }


    })

}

