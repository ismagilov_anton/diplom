module.exports = (sequelize, type) => {
    return sequelize.define('script', {
        script_id: {
            type: type.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        user_id: {
            type: type.INTEGER,
            allowNull: false
        },
        script_description: {
            type: type.CHAR,
            allowNull: false
        },
        script_type: {
            type: type.CHAR,
            allowNull: false
        },
        script_datetime: {
            type: type.DATE,
            allowNull: true
        },
        script_comparison: {
            type: type.CHAR,
            allowNull: true
        },
        script_if: {
            type: type.CHAR,
            allowNull: true
        },
        script_if_that: {
            type: type.CHAR,
            allowNull: true
        },
        script_than: {
            type: type.CHAR,
            allowNull: true
        },
        script_than_that: {
            type: type.CHAR,
            allowNull: true
        },
    })
}