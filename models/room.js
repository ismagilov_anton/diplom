
module.exports = (sequelize, type) => {
    const TIMESTAMP = require('sequelize-mysql-timestamp')(sequelize);


    return sequelize.define('room', {
        room_id: {
            type: type.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        room_name: {
            type: type.CHAR,
            allowNull: false
        },
        air_temperature_optimal_down: {
            type: type.CHAR,
            allowNull: false
        },
        air_temperature_optimal_up: {
            type: type.CHAR,
            allowNull: false
        },
        air_temperature_acceptable_down: {
            type: type.CHAR,
            allowNull: false
        },
        air_temperature_acceptable_up: {
            type: type.CHAR,
            allowNull: false
        },
        result_temperature_optimal_down: {
            type: type.CHAR,
            allowNull: false
        },
        result_temperature_optimal_up: {
            type: type.CHAR,
            allowNull: false
        },
        result_temperature_acceptable_down: {
            type: type.CHAR,
            allowNull: false
        },
        result_temperature_acceptable_up: {
            type: type.CHAR,
            allowNull: false
        },
        relative_humidity_optimal_down: {
            type: type.CHAR,
            allowNull: false
        },
        relative_humidity_optimal_up: {
            type: type.CHAR,
            allowNull: false
        },
        relative_humidity_acceptable_down: {
            type: type.CHAR,
            allowNull: false
        },
        relative_humidity_acceptable_up: {
            type: type.CHAR,
            allowNull: false
        },
        air_speed_optimal_down: {
            type: type.FLOAT,
            allowNull: false
        },
        air_speed_optimal_up: {
            type: type.FLOAT,
            allowNull: false
        },
        air_speed_acceptable_down: {
            type: type.FLOAT,
            allowNull: false
        },
        air_speed_acceptable_up: {
            type: type.FLOAT,
            allowNull: false
        }
    })
}


