module.exports = (sequelize, type) => {
    return sequelize.define('cron', {
        cron_id: {
            type: type.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        script_id: {
            type: type.INTEGER,
            allowNull: false
        },
        cron_dayOfWeek: {
            type: type.CHAR,
            allowNull: false
        },
        cron_month: {
            type: type.CHAR,
            allowNull: false
        },
        cron_dayOfMonth: {
            type: type.INTEGER,
            allowNull: false
        },
        cron_year: {
            type: type.INTEGER,
            allowNull: false
        },
        cron_hour: {
            type: type.INTEGER,
            allowNull: false
        },
        cron_minute: {
            type: type.INTEGER,
            allowNull: false
        },
        cron_utc: {
            type: type.CHAR,
            allowNull: false
        }
    })
}