module.exports = (sequelize, type) => {
    return sequelize.define('device', {
        device_id: {
            type: type.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        device_name: {
            type: type.CHAR,
            allowNull: true
        },
        device_description: {
            type: type.CHAR,
            allowNull: true
        },
        device_room: {
            type: type.CHAR,
            allowNull: true
        },
        device_topic: {
            type: type.CHAR,
            allowNull: false
        },
        device_type: {
            type: type.CHAR,
            allowNull: false
        },
        user_id: {
            type: type.INTEGER,
            allowNull: false
        }
    })
}