
module.exports = (sequelize, type) => {
    const TIMESTAMP = require('sequelize-mysql-timestamp')(sequelize);


    return sequelize.define('log', {
        log_id: {
            type: type.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        device_id: {
            type: type.INTEGER,
            allowNull: false
        },
        log_value: {
            type: type.CHAR,
            allowNull: false
        },
        log_date: {
            type: TIMESTAMP,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
            allowNull: false
        }
    })
}


