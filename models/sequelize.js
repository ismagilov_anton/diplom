const Sequelize = require('sequelize')

const DeviceModel = require('./device')
const UserModel = require('./user')
const LogModel = require('./log')
const ScriptModel = require('./script')
const CronModel = require('./cron')
const RoomModel = require('./room')

const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
    dialect: process.env.DB_DIALECT,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    define: {
        timestamps: false
    },
    logging: false // Вывод в console.log
});

const Device = DeviceModel(sequelize, Sequelize)
const User = UserModel(sequelize, Sequelize)

const Log = LogModel(sequelize, Sequelize)

const Script = ScriptModel(sequelize, Sequelize)

const Cron = CronModel(sequelize, Sequelize)
const Room = RoomModel(sequelize, Sequelize)




// Для синхронищации force: true
sequelize.sync({ })
    .then(() => {
        console.log('✅ [SEQ] Syncronisation DB success')
    })

module.exports = {
    Device,
    Log,
    Script,
    User,
    Cron,
    Room
}
