const { Device } = require('../models/sequelize')

const mqttService = require("../services/mqtt");


// Получить все скрипты
exports.getDevice = function (request, response) {
    Device.findAll({
        raw: true,
        where: {
            user_id: request.session.passport.user
        }
    }).then(devices => {
        response.render("DeviceList.hbs", {
            devices: devices
        });
    }).catch(err => console.log(err));

};

// Удаление устройства
exports.deleteDeviceId = function (request, response) {
    Device.findOne({
        where:
        {
            device_id: request.params.device_id
        }
    }).then(device => {
        console.log(device.device_topic);
        var topic = device.device_topic
        mqttService.unsubscribeTopic(topic);
        Device.destroy({
            where: {
                device_id: request.params.device_id
            }
        }).then((res) => {

            response.redirect("/device");
        });
    }).catch(err => console.log(err));
};

// Измнение скрипта
exports.editDeviceId = function (request, response) {
    Device.findOne({
        where:
        {
            device_id: request.params.device_id
        }
    }).then((device) => {
        response.render("DeviceEdit.hbs", {
            device: device
        });
    }).catch (err => console.log(err));
};

exports.editDevice = function (request, response) {
    if (!request.body) return response.sendStatus(400);

    Device.update({
        device_name: request.body.device_name,
        device_description: request.body.device_description,
        device_room: request.body.device_room,
    }, {
        where: {
            device_id: request.body.device_id
        }
    }).then((res) => {
        console.log(res);
        response.redirect("/device");
    });
};