const { Script, Device, Cron } = require('../models/sequelize')

const cronFunction = require("../services/cron");

const fs = require('fs');


var content = fs.readFileSync("device-support.json", "utf8");
var device_support = JSON.parse(content);


var sensors = [];
var actuators = [];



for (var i in device_support.devices.sensor)
    sensors.push(device_support.devices.sensor[i].hello.device_type);
for (var i in device_support.devices.actuator)
    actuators.push(device_support.devices.actuator[i].hello.device_type);


// Получить все скрипты
exports.getScript = function (request, response) {
    Script.findAll({
        raw: true,
        where: {
            user_id: request.session.passport.user
        }
    }).then(scripts => {
        response.render("ScriptList.hbs", {
            scripts: scripts
        });
    }).catch(err => console.log(err));
};

// Создание скрипта
exports.createScript = function (request, response) {
    Device.findAll({
        raw: true,
        where: {
            device_type: sensors
        }
    }).then(sensors => {
        Device.findAll({
            raw: true,
            where: {
                device_type: actuators
            }
        }).then(actuators => {
            // console.log(sensors, actuators);
            response.render("ScriptAdd.hbs", {
                sensors: sensors,
                actuators: actuators
            });
        }).catch(err => console.log(err));
    }).catch(err => console.log(err));
};

exports.createScriptPost = function (request, response) {
    if (!request.body) return response.sendStatus(400);

    Script.create({
        user_id: request.session.passport.user,
        script_description: request.body.script_description,
        script_type: request.body.script_type,
        script_datetime: request.body.script_datetime,
        script_comparison: request.body.script_comparison,
        script_if: request.body.script_if,
        script_if_that: request.body.script_if_that,
        script_than: request.body.script_than,
        script_than_that: request.body.script_than_that,
    }).then(script => {

        switch (request.body.script_type) {
            case 'datetime':
                cronFunction.createCronPost(script.dataValues);
                response.redirect('/script');
                break;

            default:
                break;
        }

        response.redirect('/script');
    }).catch(err => console.log(err));
};




// Удаление скрипта
exports.deleteScriptId = function (request, response) {
    Script.destroy({
        where: {
            script_id: request.params.script_id
        }
    }).then((res) => {
        switch (request.body.script_type) {
            case 'datetime':
                cronFunction.deleteCronId(request.params.script_id);
                response.redirect('/script');
                break;

            default:
                break;
        }


        response.redirect("/script");
    });
};

// Измнение скрипта
exports.editScriptId = function (request, response) {
    Device.findAll({
        raw: true,
        where: {
            device_type: sensors
        }
    }).then(sensors => {
        Device.findAll({
            raw: true,
            where: {
                device_type: actuators
            }
        }).then(actuators => {
            Script.findOne({
                where:
                {
                    script_id: request.params.script_id
                }
            }).then(script => {
                response.render("ScriptEdit.hbs", {
                    script: script,
                    sensors: sensors,
                    actuators: actuators
                });
            }).catch(err => console.log(err));
        }).catch(err => console.log(err));
    }).catch(err => console.log(err));
};

exports.editScript = function (request, response) {
    if (!request.body) return response.sendStatus(400);

    Script.update({
        script_description: request.body.script_description,
        script_datetime: request.body.script_datetime,
        script_type: request.body.script_type,
        script_if: request.body.script_if,
        script_if_that: request.body.script_if_that,
        script_than: request.body.script_than,
        script_than_that: request.body.script_than_that
    }, {
        where: {
            script_id: request.body.script_id
        }
    }).then((script) => {
        switch (request.body.script_type) {
            case 'datetime':
                cronFunction.editCron(request.body.script_id)
                response.redirect('/script');
                break;

            default:
                break;
        }
        response.redirect("/script");
    });
};