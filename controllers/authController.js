var exports = module.exports = {}

exports.signup = function (require, response) {
    response.render('AuthSignup.hbs', { message: require.flash('error') });
}

exports.signin = function (require, response) {
    response.render('AuthSignin.hbs', { message: require.flash('error') });
}
exports.index = function (request, response) {
    response.render("AuthIndex.hbs");
};

exports.logout = function (req, res) {
    req.session.destroy(function (err) {
        res.redirect('/');
    });
}

