const { Log, Device } = require('../models/sequelize')

const fs = require('fs');

var content = fs.readFileSync("device-support.json", "utf8");
var device_support = JSON.parse(content);


var sensors = [];



for (var i in device_support.devices.sensor)
    sensors.push(device_support.devices.sensor[i].hello.device_type);


exports.getLog = function (request, response) {

    Device.findAll({
        raw: true,
        where: {
            user_id: request.session.passport.user,
            device_type: sensors
        }
    }).then(sensors => {
        // console.log(sensors);
        response.render("LogList.hbs", {
            sensors: sensors
        });
    }).catch(err => console.log(err));
};

exports.getLogId = function (request, response) {
    // console.log(request);

    Log.findAll({
        raw: true,
        where: {
            device_id: request.params.id
        }
    }).then(logs => {
        Device.findOne({
            raw: true,
            where: {
                device_id: request.params.id
            }
        }).then(device => {
            // console.log(device);
            console.log(logs);


            var value_array = [];
            var datetime_array = [];

            logs.forEach(element => {
                //  console.log(element.log_value + " " + element.log_date.getHours());
                value_array.push(element.log_value);
                datetime_array.push(element.log_date.getTime());

            });

            // console.log(value_array);
            console.table(datetime_array);
            // console.table(datetime_array.sort());


            response.render("LogData.hbs", {
                device: device,
                values: value_array,
                datetime: datetime_array
            });
        }).catch(err => console.log(err));
    }).catch(err => console.log(err));
};