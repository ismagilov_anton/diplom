const { User, Device, Script } = require('../models/sequelize')

exports.profile = function (request, response) {
    User.findOne({
        raw: true,
        where: {
            id: request.session.passport.user
        }
    }).then(user => {
        Device.findAll({
            raw: true,
            where: {
                user_id: request.session.passport.user
            }
        }).then(devices => {
            Script.findAll({
                raw: true,
                where: {
                    user_id: request.session.passport.user
                }
            }).then(scripts => {
                response.render("HomeProfile.hbs", {
                    user: user,
                    device: devices.length,
                    script: scripts.length
                });
            }).catch(err => console.log(err));
        }).catch(err => console.log(err));
    }).catch(err => console.log(err));
}

exports.main = function (request, response) {
    response.render("HomeMain.hbs");
};

exports.test = function (request, response) {
    response.render("HomeTest.hbs");
};
